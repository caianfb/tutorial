import kivy
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label

class FirstLayout(BoxLayout):
    pass


class Test(App):
    def build(self):
        return FirstLayout()

Test().run()
